# SPDX-FileCopyrightText: Copyright 2023 xlogic <https://xlogic.dev> and contributors to the project
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileContributor: 2023 Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>

# Enable all rules
all

# Extend line length rule to 140 characters
# https://github.com/markdownlint/markdownlint/blob/main/docs/RULES.md#md013---line-length
rule 'MD013', :line_length => 140
