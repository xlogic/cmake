// SPDX-FileCopyrightText: Copyright 2023 xlogic <https://xlogic.dev> and contributors to the project
// SPDX-License-Identifier: Apache-2.0
// SPDX-FileContributor: 2023 Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>

module example;
    import "DPI-C" function int dpi_call_1();
    import "DPI-C" function int dpi_call_2();

    initial begin: main
        $display("Hello %0d %0d", dpi_call_1(), dpi_call_2());
    end

    a x();
endmodule
