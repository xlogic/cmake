# SPDX-FileCopyrightText: Copyright 2023 xlogic <https://xlogic.dev> and contributors to the project
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileContributor: 2023 Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>

if(NOT LANG)
  set(LANG HDL)
endif()

set(CMAKE_DEFINE_FLAG_${LANG}           "+define+")
set(CMAKE_INCLUDE_FLAG_${LANG}          "+incdir+")
set(CMAKE_STATIC_LIBRARY_PREFIX_${LANG} "")
set(CMAKE_STATIC_LIBRARY_SUFFIX_${LANG} ".vlib")
set(CMAKE_SHARED_LIBRARY_PREFIX_${LANG} "")
set(CMAKE_SHARED_LIBRARY_SUFFIX_${LANG} ".vlib")
set(CMAKE_${LANG}_LINK_LIBRARY_FLAG     "-l")
set(CMAKE_${LANG}_LIBRARY_PATH_FLAG     "-L")

include(Compiler/${CMAKE_${LANG}_COMPILER_ID} OPTIONAL)
include(Compiler/${CMAKE_${LANG}_COMPILER_ID}-${LANG} OPTIONAL)

# Compile a HDL file into an object file
if(NOT CMAKE_${LANG}_COMPILE_OBJECT)
  set(CMAKE_${LANG}_COMPILE_OBJECT
    "<CMAKE_${LANG}_COMPILER> -o <OBJECT> <DEFINES> <INCLUDES> <FLAGS> <SOURCE>"
  )
endif()

# Create a HDL library
if(NOT CMAKE_${LANG}_CREATE_STATIC_LIBRARY)
  set(CMAKE_${LANG}_CREATE_STATIC_LIBRARY
    "<CMAKE_${LANG}_COMPILER> -o <TARGET> <LINK_FLAGS> <OBJECTS>"
  )
endif()

# Create a HDL library
if(NOT CMAKE_${LANG}_CREATE_SHARED_LIBRARY)
  set(CMAKE_${LANG}_CREATE_SHARED_LIBRARY
    "<CMAKE_${LANG}_COMPILER> -o <TARGET> <CMAKE_SHARED_LIBRARY_${LANG}_FLAGS> <LANGUAGE_COMPILE_FLAGS> <LINK_FLAGS> <CMAKE_SHARED_LIBRARY_CREATE_${LANG}_FLAGS> <SONAME_FLAG><TARGET_SONAME> <OBJECTS> <LINK_LIBRARIES>"
  )
endif()

# Elaborate HDL and create HDL simulator executable
if(NOT CMAKE_${LANG}_LINK_EXECUTABLE)
  set(CMAKE_${LANG}_LINK_EXECUTABLE
    "<CMAKE_${LANG}_COMPILER> -o <TARGET> <FLAGS> <CMAKE_${LANG}_LINK_FLAGS> <LINK_FLAGS> <OBJECTS> <LINK_LIBRARIES>"
  )
endif()

unset(LANG)
