# SPDX-FileCopyrightText: Copyright 2023 xlogic <https://xlogic.dev> and contributors to the project
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileContributor: 2023 Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>

# Determine HDL (Hardware Description Language) compiler identifier and version.

# Sets the following variables:
#   CMAKE_${LANG}_COMPILER_ID
#   CMAKE_${LANG}_COMPILER_VERSION
#   CMAKE_${LANG}_ELABORATOR
#   CMAKE_${LANG}_SIMULATOR

function(cmake_determine_hdl_compiler lang)
  # Get HDL compiler name without directory path and file extension
  get_filename_component(compiler_name "${CMAKE_${lang}_COMPILER}" NAME_WE)

  # Get directory path to HDL compiler to determine other HDL tools
  get_filename_component(compiler_dir "${CMAKE_${lang}_COMPILER}" DIRECTORY)

  # Get clean HDL compiler name
  string(TOLOWER "${compiler_name}" compiler_name)
  string(STRIP   "${compiler_name}" compiler_name)

  # HDL compiler output
  set(compiler_output)

  # HDL elaborator
  set(elaborator_names)
  set(elaborator_hints)

  # HDL simulator
  set(simulator_names)
  set(simulator_hints)

  # Get HDL compiler standard output to determine compiler name and compiler version
  if(CMAKE_${lang}_COMPILER AND NOT CMAKE_${lang}_COMPILER_ID OR NOT CMAKE_${lang}_COMPILER_VERSION)
    set(compiler_arguments "")

    if("${compiler_name}" MATCHES "^(vlog|vcom)$")
      set(compiler_arguments "-version")
    elseif("${compiler_name}" MATCHES "^(xvlog|xvhdl)$")
      set(compiler_arguments "--nolog --version")
    endif()

    # Get HDL compiler identifier and version
    if(compiler_arguments)
      execute_process(
        COMMAND "${CMAKE_${lang}_COMPILER}" ${compiler_arguments}
        RESULT_VARIABLE compiler_result
        OUTPUT_VARIABLE compiler_output
        ERROR_VARIABLE  compiler_output
        WORKING_DIRECTORY "${CMAKE_BINARY_DIR}"
        TIMEOUT 10
      )
    endif()

    unset(compiler_arguments)
  endif()

  # Get clean HDL compiler output
  string(TOLOWER "${compiler_output}" compiler_output)
  string(STRIP   "${compiler_output}" compiler_output)

  # Get HDL compiler identifier
  if(NOT CMAKE_${lang}_COMPILER_ID)
    if("${compiler_output}" MATCHES "^.*questa.*$")
      set(CMAKE_${lang}_COMPILER_ID Questa)
      set(simulator_names vsim)

    elseif("${compiler_output}" MATCHES "^.*vivado.*simulator.*$")
      set(CMAKE_${lang}_COMPILER_ID VivadoSimulator)
      set(elaborator_names xelab)
      set(simulator_names xsim)

    endif()
  endif()

  # Get HDL compiler version
  if(NOT CMAKE_${lang}_COMPILER_VERSION)
    # Get HDL compiler version from standard output
    string(REGEX MATCH "[0-9]+\.[0-9]+(\.[0-9]+)?(\.[0-9]+)?" CMAKE_${lang}_COMPILER_VERSION "${compiler_output}")
  endif()

  # Find HDL elaborator tool
  if(NOT CMAKE_${lang}_ELABORATOR AND elaborator_names)
    find_program(CMAKE_${lang}_ELABORATOR NAMES ${elaborator_names} HINTS ${compiler_dir} ${elaborator_hints})
  endif()

  # Find HDL simulator tool
  if(NOT CMAKE_${lang}_SIMULATOR AND simulator_names)
    find_program(CMAKE_${lang}_SIMULATOR NAMES ${simulator_names} HINTS ${compiler_dir} ${simulator_hints})
  endif()

  # Display the final identification result
  if(CMAKE_${lang}_COMPILER_ID)
    if(CMAKE_${lang}_COMPILER_VERSION)
      set(_version " ${CMAKE_${lang}_COMPILER_VERSION}")
    else()
      set(_version "")
    endif()

    message(STATUS "The ${lang} compiler identification is ${CMAKE_${lang}_COMPILER_ID}${_version}")
    unset(_version)
  else()
    message(STATUS "The ${lang} compiler identification is unknown")
  endif()

  unset(compiler_name)
  unset(compiler_dir)
  unset(compiler_output)
  unset(compiler_result)
  unset(elaborator_names)
  unset(elaborator_hints)
  unset(simulator_names)
  unset(simulator_hints)

  set(CMAKE_${lang}_COMPILER_ID "${CMAKE_${lang}_COMPILER_ID}" PARENT_SCOPE)
  set(CMAKE_${lang}_COMPILER_VERSION "${CMAKE_${lang}_COMPILER_VERSION}" PARENT_SCOPE)
endfunction()
