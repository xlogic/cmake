#!/usr/bin/env python3
#
# SPDX-FileCopyrightText: Copyright 2024 xlogic <https://xlogic.dev> and contributors to the project
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileContributor: 2024 Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>

import sys
import pathlib
import argparse
import subprocess

from filelock import FileLock

def main():
    parser = argparse.ArgumentParser(add_help=False)
    parser.add_argument('--compiler', default='xvlog')
    parser.add_argument('--work', '-work', default='work')
    parser.add_argument('--workdir', type=pathlib.Path, default='.')
    parser.add_argument('-o', '--output', type=pathlib.Path, default='a.vo')

    args, remaining = parser.parse_known_args()

    lib = args.workdir / 'xsim.dir' / args.work

    if not lib.exists():
        lib.mkdir(parents=True, exist_ok=True)

    returncode = 0
    lock = FileLock(lib / '.lock')

    command = [args.compiler, '--work', args.work]
    command.extend(remaining)

    with lock:
        # Run: vlog|vcom ...
        returncode = subprocess.run(command, cwd=args.workdir).returncode

    if returncode == 0:
        args.output.touch()

    sys.exit(returncode)

if __name__ == '__main__':
    main()
