#!/usr/bin/env python3
#
# SPDX-FileCopyrightText: Copyright 2023 xlogic <https://xlogic.dev> and contributors to the project
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileContributor: 2023 Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>

import sys
import pathlib
import argparse
import datetime
import subprocess

def main():
    parser = argparse.ArgumentParser(add_help=False)
    parser.add_argument('--elaborator', default='xelab')
    parser.add_argument('--simulator', default='xsim')
    parser.add_argument('--workdir', type=pathlib.Path, default='.')
    parser.add_argument('--top')
    parser.add_argument('-o', dest='output', type=pathlib.Path, default='a.out')

    args, remaining = parser.parse_known_args()

    # Create elaborated snapshot. Add the snapshot. prefix to avoid name clashing from HDL libraries
    snapshot = 'snapshot.' + args.output.stem

    # Top design unit name. If not provided, extracted from output file name (target)
    top = args.top if args.top else args.output.stem

    # Compose command line arguments Xilinx Vivado Simulator xelab to generate elaborated design
    command = [args.elaborator, '--snapshot', snapshot]

    clib_paths = {}

    for arg in remaining:
        if arg.endswith('.vlib'):
            with pathlib.Path(arg).open('r', encoding='utf-8') as file:
                command.extend(file.read().splitlines())

        elif arg.endswith('.svo') or arg.endswith('.vo') or arg.endswith('.vhdo'):
            pass

        else:
            command.append(arg)

    command.append(top)

    # Run: vsim -elab <elab_file> ... <top>
    p = subprocess.run(command, cwd=args.output.parent)

    if p.returncode:
        sys.exit(p.returncode)

    # Create standalone executable as wrapper around Questa vsim simulator using elaborated design
    with pathlib.Path(args.output).open('w', encoding='utf-8') as file:
        file.write('''#!/usr/bin/env python3
#
# SPDX-FileCopyrightText: Copyright {year} xlogic <https://xlogic.dev> and contributors to the project
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileContributor: {year} Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>

import sys
import pathlib
import argparse
import subprocess

from filelock import FileLock

def main():
    parser = argparse.ArgumentParser(add_help=False)
    parser.add_argument('--xsimdir', '-xsimdir', type=pathlib.Path, default='{xsimdir}')
    parser.add_argument('--snapshot', '-snapshot', default='{snapshot}')

    args, remaining = parser.parse_known_args()

    command = ['{simulator}', '--runall', '--xsimdir', args.xsimdir]
    command.extend(remaining)
    command.append(args.snapshot)

    returncode = 0
    lock = FileLock(args.xsimdir / 'xsim.dir' / args.snapshot / '.lock')

    with lock:
        returncode = subprocess.run(command).returncode

    sys.exit(returncode)

if __name__ == '__main__':
    main()
'''.format(year=datetime.date.today().year, xsimdir=args.workdir, simulator=args.simulator, snapshot=snapshot))

    pathlib.Path(args.output).chmod(0o755)

if __name__ == '__main__':
    main()
