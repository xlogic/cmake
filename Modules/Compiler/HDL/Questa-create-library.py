#!/usr/bin/env python3
#
# SPDX-FileCopyrightText: Copyright 2024 xlogic <https://xlogic.dev> and contributors to the project
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileContributor: 2024 Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>

import sys
import pathlib
import argparse

def main():
    parser = argparse.ArgumentParser(add_help=False)
    parser.add_argument('-o', dest='output', default='a.vlib')

    args, _ = parser.parse_known_args()

    pathlib.Path(args.output).touch()

if __name__ == '__main__':
    main()
