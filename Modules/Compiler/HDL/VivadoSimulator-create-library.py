#!/usr/bin/env python3
#
# SPDX-FileCopyrightText: Copyright 2024 xlogic <https://xlogic.dev> and contributors to the project
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileContributor: 2024 Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>

import sys
import pathlib
import argparse

def main():
    parser = argparse.ArgumentParser(add_help=False)
    parser.add_argument('--work', '-work')
    parser.add_argument('-o', '--output', type=pathlib.Path, default='a.vlib')

    args, _ = parser.parse_known_args()

    lib = args.work if args.work else args.output.stem

    items = ['--lib', lib]

    # Create library
    with pathlib.Path(args.output).open('w', encoding='utf-8') as file:
        file.writelines('\n'.join(items))

if __name__ == '__main__':
    main()
