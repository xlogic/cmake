#!/usr/bin/env python3
#
# SPDX-FileCopyrightText: Copyright 2024 xlogic <https://xlogic.dev> and contributors to the project
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileContributor: 2024 Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>

import sys
import pathlib
import argparse
import subprocess

from filelock import FileLock

def main():
    parser = argparse.ArgumentParser(add_help=False)
    parser.add_argument('--compiler', default='vlog')
    parser.add_argument('-work', default='work')
    parser.add_argument('-o', dest='output', default='a.vo')

    args, remaining = parser.parse_known_args()

    command = [args.compiler, '-work', args.work]
    command.extend(remaining)

    returncode = 0
    lock = FileLock(args.work + '.lock')

    with lock:
        # Run: vlog|vcom ...
        returncode = subprocess.run(command).returncode

    if returncode == 0:
        pathlib.Path(args.output).touch()

    sys.exit(returncode)

if __name__ == '__main__':
    main()
