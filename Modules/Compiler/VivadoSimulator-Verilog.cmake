# SPDX-FileCopyrightText: Copyright 2024 xlogic <https://xlogic.dev> and contributors to the project
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileContributor: 2024 Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>

set(CMAKE_Verilog_DEFINE_FLAG  "+define+")
set(CMAKE_Verilog_INCLUDE_FLAG "+incdir+")

# Compile a Verilog file into an object file
if(NOT CMAKE_Verilog_COMPILE_OBJECT)
  set(CMAKE_Verilog_COMPILE_OBJECT
    "\"${CMAKE_CURRENT_LIST_DIR}/HDL/VivadoSimulator-compile-object.py\" --compiler <CMAKE_Verilog_COMPILER> --workdir \"${CMAKE_BINARY_DIR}\" -o <OBJECT> <DEFINES> <INCLUDES> <FLAGS> <SOURCE>"
  )
endif()
