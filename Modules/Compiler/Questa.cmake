# SPDX-FileCopyrightText: Copyright 2024 xlogic <https://xlogic.dev> and contributors to the project
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileContributor: 2023 Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>

# Create a HDL library
if(NOT CMAKE_${LANG}_CREATE_STATIC_LIBRARY)
  set(CMAKE_${LANG}_CREATE_STATIC_LIBRARY
    "\"${CMAKE_CURRENT_LIST_DIR}/HDL/Questa-create-library.py\" -o <TARGET> <LINK_FLAGS> <OBJECTS>"
  )
endif()

# Create a HDL library
if(NOT CMAKE_${LANG}_CREATE_SHARED_LIBRARY)
  set(CMAKE_${LANG}_CREATE_SHARED_LIBRARY
    "\"${CMAKE_CURRENT_LIST_DIR}/HDL/Questa-create-library.py\" -o <TARGET> <CMAKE_SHARED_LIBRARY_${LANG}_FLAGS> <LANGUAGE_COMPILE_FLAGS> <LINK_FLAGS> <CMAKE_SHARED_LIBRARY_CREATE_${LANG}_FLAGS> <SONAME_FLAG><TARGET_SONAME> <OBJECTS> <LINK_LIBRARIES>"
  )
endif()

# Create a HDL simulator executable
if(NOT CMAKE_${LANG}_LINK_EXECUTABLE)
  set(CMAKE_${LANG}_LINK_EXECUTABLE
    "\"${CMAKE_CURRENT_LIST_DIR}/HDL/Questa-link-executable.py\" --simulator \"${CMAKE_${LANG}_SIMULATOR}\" -o <TARGET> <FLAGS> <CMAKE_${LANG}_LINK_FLAGS> <LINK_FLAGS> <OBJECTS> <LINK_LIBRARIES>"
  )
endif()
