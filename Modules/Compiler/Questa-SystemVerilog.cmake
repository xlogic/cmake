# SPDX-FileCopyrightText: Copyright 2023 xlogic <https://xlogic.dev> and contributors to the project
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileContributor: 2023 Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>

set(CMAKE_SystemVerilog_DEFINE_FLAG  "+define+")
set(CMAKE_SystemVerilog_INCLUDE_FLAG "+incdir+")

# Compile a SystemVerilog file into an object file
if(NOT CMAKE_SystemVerilog_COMPILE_OBJECT)
  set(CMAKE_SystemVerilog_COMPILE_OBJECT
    "\"${CMAKE_CURRENT_LIST_DIR}/HDL/Questa-compile-object.py\" --compiler <CMAKE_SystemVerilog_COMPILER> -o <OBJECT> <DEFINES> <INCLUDES> <FLAGS> <SOURCE>"
  )
endif()
