# SPDX-FileCopyrightText: Copyright 2023 xlogic <https://xlogic.dev> and contributors to the project
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileContributor: 2023 Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>

set(CMAKE_VHDL_DEFINE_FLAG)
set(CMAKE_VHDL_INCLUDE_FLAG)

# Compile a VHDL file into an object file
if(NOT CMAKE_VHDL_COMPILE_OBJECT)
  set(CMAKE_VHDL_COMPILE_OBJECT
    "\"${CMAKE_CURRENT_LIST_DIR}/HDL/Questa-compile-object.py\" --compiler <CMAKE_VHDL_COMPILER> -o <OBJECT> <FLAGS> <SOURCE>"
  )
endif()
