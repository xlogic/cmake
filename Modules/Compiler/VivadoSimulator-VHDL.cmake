# SPDX-FileCopyrightText: Copyright 2024 xlogic <https://xlogic.dev> and contributors to the project
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileContributor: 2024 Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>

set(CMAKE_VHDL_DEFINE_FLAG)
set(CMAKE_VHDL_INCLUDE_FLAG)

# Compile a VHDL file into an object file
if(NOT CMAKE_VHDL_COMPILE_OBJECT)
  set(CMAKE_VHDL_COMPILE_OBJECT
    "\"${CMAKE_CURRENT_LIST_DIR}/HDL/VivadoSimulator-compile-object.py\" --compiler <CMAKE_VHDL_COMPILER> --workdir \"${CMAKE_BINARY_DIR}\" -o <OBJECT> <DEFINES> <INCLUDES> <FLAGS> <SOURCE>"
  )
endif()
