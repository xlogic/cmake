#!/usr/bin/env python3
#
# SPDX-FileCopyrightText: Copyright 2024 xlogic <https://xlogic.dev> and contributors to the project
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileContributor: 2024 Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>

import sys
import pathlib
import argparse
import subprocess

def main():
    parser = argparse.ArgumentParser(add_help=False)
    parser.add_argument('--compiler', default='xvlog')
    parser.add_argument('--sv', action='store_true')
    parser.add_argument('-o', dest='output', default='a.vo')

    args, remaining = parser.parse_known_args()

    for arg in reversed(remaining):
        if arg.endswith('.sv'):
            args.sv = True
            break

    command = [args.compiler]
    command.extend(remaining)

    # Run: vlog|vcom ...
    p = subprocess.run(command)

    if p.returncode:
        sys.exit(p.returncode)

    with pathlib.Path(args.output).open('w', encoding='utf-8') as file:
        file.writelines('\n'.join(command))

    pathlib.Path(args.output).chmod(0o644)

if __name__ == '__main__':
    main()
