#!/usr/bin/env python3
#
# SPDX-FileCopyrightText: Copyright 2024 xlogic <https://xlogic.dev> and contributors to the project
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileContributor: 2024 Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>

import sys
import pathlib
import argparse

def main():
    parser = argparse.ArgumentParser(add_help=False)
    parser.add_argument('--compiler', default='xvlog')
    parser.add_argument('-o', dest='output', default='a.vlib')

    args, remaining = parser.parse_known_args()

    arguments = []

    with pathlib.Path(args.output).open('w', encoding='utf-8') as file:
        file.writelines('\n'.join(arguments))

    pathlib.Path(args.output).chmod(0o644)

if __name__ == '__main__':
    main()
