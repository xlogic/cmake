#!/usr/bin/env python3
#
# SPDX-FileCopyrightText: Copyright 2023 xlogic <https://xlogic.dev> and contributors to the project
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileContributor: 2023 Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>

import sys
import pathlib
import argparse
import datetime
import subprocess

def main():
    parser = argparse.ArgumentParser(add_help=False)
    parser.add_argument('--compiler', default='xvlog')
    parser.add_argument('--top')
    parser.add_argument('-o', dest='output', default='a.out')

    args, remaining = parser.parse_known_args()

    # Get toolchain path location based on Questa vlog|vcom compiler
    toolchain = pathlib.Path(args.compiler).parent

    # Questa vsim command line tool
    vsim = 'xsim' if toolchain == pathlib.Path() else toolchain / 'xsim'

    # Base name
    name = pathlib.Path(args.output).stem

    # Output elaboration file
    elab = pathlib.Path(args.output).parent / (name + '.elab')

    # Top design unit name. If not provided, extracted from output file name (target)
    top = args.top if args.top else name

    # Compose command line arguments for Questa vsim simulator to generate elaborated design
    command = [vsim, '-batch', '-elab', elab]

    vlib_paths = {}
    vlib_names = {}

    clib_paths = {}
    clib_names = {}

    for arg in remaining:
        if arg.endswith('.vlib'):
            file = pathlib.Path(arg)
            path = file.parent
            name = file.stem

            if path not in vlib_paths:
                vlib_paths[path] = True
                command.extend(['-Ldir', path])

            if name not in vlib_names:
                vlib_names[name] = True
                command.extend(['-L', name])

        elif arg.endswith('.so') or arg.endswith('.a'):
            file = pathlib.Path(arg)
            path = file.parent.resolve()
            name = file.stem[3:]

            if path not in clib_paths:
                clib_paths[path] = True
                command.extend(['-ldflags', '-Wl,--rpath={0} -L{0}'.format(path)])

            if name not in clib_names:
                clib_names[name] = True
                command.extend(['-ldflags', '-l{}'.format(name)])

        elif arg.startswith(('-Wl', '-L', '-l')) and len(arg) > 2:
            command.extend(['-ldflags', arg])

        elif arg.endswith('.svo') or arg.endswith('.vo') or arg.endswith('.vhdo'):
            pass

        else:
            command.append(arg)

    command.append(top)

    # Run: vsim -elab <elab_file> ... <top>
    p = subprocess.run(command)

    if p.returncode:
        sys.exit(p.returncode)

    # Create standalone executable as wrapper around Questa vsim simulator using elaborated design
    with pathlib.Path(args.output).open('w', encoding='utf-8') as file:
        file.write('''#!/usr/bin/env python3
#
# SPDX-FileCopyrightText: Copyright {year} xlogic <https://xlogic.dev> and contributors to the project
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileContributor: {year} Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>

import sys
import argparse
import subprocess

def main():
    parser = argparse.ArgumentParser(add_help=False)
    parser.add_argument('-load_elab', default='{elab}')
    parser.add_argument('-do', default='run -all;exit')

    args, remaining = parser.parse_known_args()

    command = ['{vsim}']
    command.extend(['-load_elab', args.load_elab, '-do', args.do])
    command.extend(remaining)

    sys.exit(subprocess.run(command).returncode)

if __name__ == '__main__':
    main()
'''.format(year=datetime.date.today().year, elab=elab.resolve(), vsim=vsim))

    pathlib.Path(args.output).chmod(0o755)

if __name__ == '__main__':
    main()
