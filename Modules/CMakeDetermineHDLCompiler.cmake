# SPDX-FileCopyrightText: Copyright 2023 xlogic <https://xlogic.dev> and contributors to the project
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileContributor: 2023 Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>

# Determine compiler for HDL (Hardware Description Language) that includes VHDL, Verilog and SystemVerilog.

# Sets the following variables:
#   CMAKE_${LANG}_COMPILER
#   CMAKE_${LANG}_COMPILER_ID
#   CMAKE_${LANG}_COMPILER_VERSION
#   CMAKE_${LANG}_ELABORATOR
#   CMAKE_${LANG}_SIMULATOR

# Possible to enable CMake project language separately per VHDL, Verilog or SystemVerilog.
if(NOT LANG)
  set(LANG HDL)
endif()

# Load system-specific compiler preferences for this language.
include(Platform/${CMAKE_SYSTEM_NAME}-Determine-${LANG} OPTIONAL)
include(Platform/${CMAKE_SYSTEM_NAME}-${LANG} OPTIONAL)

# Names of potential HDL compilers
if(NOT CMAKE_${LANG}_COMPILER_NAMES)
  set(CMAKE_${LANG}_COMPILER_NAMES vlog vcom xvlog xvhdl)
endif()

# Paths to potential HDL compilers
if(NOT CMAKE_${LANG}_COMPILER_PATHS)
    set(CMAKE_${LANG}_COMPILER_PATHS
      $ENV{MODEL_TECH}
      $ENV{VIVADO_PATH}
      $ENV{MYVIVADO}
    )
endif()

if(NOT CMAKE_${LANG}_COMPILER)
  set(CMAKE_${LANG}_COMPILER_INIT NOTFOUND)

  # Prefer the environment variable
  if(NOT $ENV{${LANG}_COMPILER} STREQUAL "")
    get_filename_component(CMAKE_${LANG}_COMPILER_INIT $ENV{${LANG}_COMPILER} PROGRAM PROGRAM_ARGS CMAKE_${LANG}_FLAGS_ENV_INIT)

    if(CMAKE_${LANG}_FLAGS_ENV_INIT)
      set(CMAKE_${LANG}_COMPILER_ARG1 "${CMAKE_${LANG}_FLAGS_ENV_INIT}" CACHE STRING "Arguments to ${LANG} compiler")
    endif()

    if(NOT EXISTS ${CMAKE_${LANG}_COMPILER_INIT})
      message(FATAL_ERROR "Could not find compiler set in environment variable ${LANG}_COMPILER:\n$ENV{${LANG}_COMPILER}.\n${CMAKE_${LANG}_COMPILER_INIT}")
    endif()
  endif()

  if(CMAKE_${LANG}_COMPILER_INIT)
    set(CMAKE_${LANG}_COMPILER "${CMAKE_${LANG}_COMPILER_INIT}" CACHE PATH "${LANG} compiler")
  else()
    find_program(
      CMAKE_${LANG}_COMPILER
      NAMES ${CMAKE_${LANG}_COMPILER_NAMES}
      HINTS ${CMAKE_${LANG}_COMPILER_HINTS}
      PATHS ${CMAKE_${LANG}_COMPILER_PATHS}
    )
  endif()
endif()

include(CMakeDetermineHDLCompilerId)
cmake_determine_hdl_compiler("${LANG}")

mark_as_advanced(CMAKE_${LANG}_COMPILER)

# Configure variables set in this file for fast reload later on
configure_file(
  "${CMAKE_CURRENT_LIST_DIR}/CMake${LANG}Compiler.cmake.in"
  "${CMAKE_PLATFORM_INFO_DIR}/CMake${LANG}Compiler.cmake"
  @ONLY
)

set(CMAKE_${LANG}_COMPILER_ENV_VAR "${LANG}_COMPILER")
string(TOUPPER "${CMAKE_${LANG}_COMPILER_ENV_VAR}" CMAKE_${LANG}_COMPILER_ENV_VAR)

unset(LANG)
